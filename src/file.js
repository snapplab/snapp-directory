let path = require('path');

const {File} = global.SnappFramework;

module.exports = function (self) {
  return File(path.join(self.path, ...this.strings));
};
