let path = require('path');

const {File, Template, MediaType, Store} = global.SnappFramework;

let templateCache = Store.Memory();

module.exports = function (self) {
  let filePath = path.join(self.path, ...this.strings);
  return templateCache.get(filePath, function () {
    let file = File(`${filePath}.html`);
    return Template(file, MediaType.html);
  });
};
