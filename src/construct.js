let path = require('path');

module.exports = function (self) {
  self.path = path.join(...this.strings);
};
