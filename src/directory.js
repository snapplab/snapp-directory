let path = require('path');

const {Directory} = global.SnappFramework;

module.exports = function (self) {
  return Directory(path.join(self.path, ...this.strings));
};
